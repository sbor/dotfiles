# Dotfiles

This repo is used for managing my dotfiles with the help of [Dotdrop](https://github.com/deadc0de6/dotdrop).

Just follow these steps to get going:

```
# fetch repo and dotdrop
git clone git@gitlab.com:sbor/dotfiles.git
cd dotfiles
git submodule update --init

# install dotdrop
pip3 install -r dotdrop/requirements.txt --user
./dotdrop/bootstrap.sh

# install a profile
./dotdrop.sh install -p <profile>

```

With zsh shell installed, the alias `dotdrop` should now be installed along with all the config files.
