" ============================
" GLOBAL SETTINGS
" ============================
" required for vim
set nocompatible              

" disable beep / flash
set vb t_vb=

" line numbers
set number
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround

" handle indentation
"set autoindent
" file type recognition
filetype indent plugin on

" clipboard is read when yanking, pasting
set clipboard+=unnamedplus
set fileformat=unix

" enable mouse clicks on tabs
set mouse=a

" case insensitive search
set ignorecase
" if starting with caps, it's case sensitive
set smartcase

" keymaps
nmap ; :
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l
let mapleader = " "

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" filetype specific stuff
" use plugin instead
"autocmd FileType python set tabstop=4 shiftwidth=4 smarttab expandtab softtabstop=4 autoindent
autocmd FileType javascript set et sta sw=2 sts=2

" move opened files to right or below
set splitbelow
set splitright

" ============================
" PLUGINS
" ============================
" auto-install vim-plug
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
	curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	" autocmd VimEnter * PlugInstall --sync | source ~/.local/share/nvim/site/autoload/plug.vim
endif

" load plugins
call plug#begin('~/.local/share/nvim/plugged')
" Status bar
Plug 'vim-airline/vim-airline'

" essential editing plugins
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
"Plug 'tpope/vim-easy-align'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-vinegar'

" Sanity features
Plug 'kien/ctrlp.vim'

" Syntax checking
Plug 'scrooloose/syntastic'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
"Plug 'JamshedVesuna/vim-markdown-preview'
"Code Completion
"Plug 'Valloric/YouCompleteMe'
" Code completion
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"Plug 'pangloss/vim-javascript'
"Plug 'leafgarland/typescript-vim'

" Rust language and code completion
"Plug 'rust-lang/rust.vim'
"Plug 'racer-rust/vim-racer'
"Plug 'altercation/vim-colors-solarized'


" Python
Plug 'ambv/black'
Plug 'fisadev/vim-isort'

" Debugging
Plug 'vim-vdebug/vdebug', {'tag': 'v1.5.2'}

" Latex
Plug 'lervag/vimtex'

" Theming
Plug 'morhetz/gruvbox'
" misc
Plug '/usr/bin/fzf'

call plug#end()


" ============================
" PLUGIN SETTINGS
" ============================
" syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:syntastic_rust_checkers = ['rustc', 'cargo']
let g:syntastic_ocaml_checkers = ['merlin']

let g:syntastic_disabled_filetypes=['html', 'tex']

" vim-markdown
let g:vim_markdown_preview_use_xdg_open = 1
"let g:vim_markdown_preview_browser = 'Google Chrome'
"let g:vim_markdown_preview_github = 1
let g:vim_markdown_preview_pandoc = 1

" prettier
let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync

" black
"autocmd BufWritePre *.py execute ':Black'

" netrw
"let g:netrw_liststyle = 3
"let g:netrw_banner = 0
"let g:netrw_browse_split = 4
"let g:netrw_altv = 1
"let g:netrw_winsize = 20
"augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Vexplore
"augroup END

" vimtex
let g:vimtex_view_method = 'zathura'
let g:vimtex_view_use_temp_files = 1
let g:vimtex_compiler_progname = 'nvr'
" syntax highlighting
syntax enable

" Set coloring
set termguicolors
let g:gruvbox_italic=1
set background=dark
colorscheme gruvbox
