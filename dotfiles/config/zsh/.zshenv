# some variables
export BROWSER=/usr/bin/firefox
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export MONITOR=HDMI1
export TERM=screen-256color

# rustup
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.cargo/bin"

# luarocks (required by mpv plugin)
export PATH="$PATH:/usr/local/bin/luarocks"

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# everything else
export PATH="$PATH:$HOME/.local/bin"

# Android SDK
export ANDROID_HOME=/home/stefan/Android/Sdk
export ANDROID_EMULATOR_HOME=/home/stefan/Android/Sdk/emulator
export ANDROID_AVD_HOME=/home/stefan/.android/avd
#export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1
export PATH="$PATH:/home/stefan/Android/Sdk/tools"
export PATH=$PATH:"/home/stefan/Android/Sdk/tools/bin"
export PATH="$PATH:/home/stefan/Android/Sdk/platform-tools"
export JAVA_HOME=/usr/lib64/openjdk-8

# experimental firefox tweaks
export MOZ_USE_XINPUT2=1
#export MOZ_WEBRENDER=1

# theming
export QT_QPA_PLATFORMTHEME=qt5ct
#export QT_STYLE_OVERRIDE=gtk3

# alacritty scaling
export WINIT_HIDPI_FACTOR=1.0
