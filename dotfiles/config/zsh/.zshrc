# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Stop Ctl+S
stty -ixon

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd #nomatch
unsetopt beep extendedglob
bindkey -e
autoload -Uz compinit promptinit
compinit
promptinit

# fzf goodies http://owen.cymru/fzf-ripgrep-navigate-with-bash-faster-than-ever-before/
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden -g "!{.git,node_modules,*.swp}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

fvim() {
  local IFS=$'\n'
  local files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

## Keybindings
# ALT + arrow
bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word
# CTRL + arrow
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

### <ZPLUG>
# load zplug, manage itself
source ~/.zplug/init.zsh
zplug 'zplug/zplug', hook-build:'zplug --self-manage'

# load plugins
zplug "plugins/docker", from:oh-my-zsh
zplug "plugins/docker-compose", from:oh-my-zsh
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/sudo", from:oh-my-zsh

zplug "plugins/rust", from:oh-my-zsh
zplug "plugins/cargo", from:oh-my-zsh

zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-completions"

# run after compinit command
zplug "zsh-users/zsh-syntax-highlighting", defer:2
zplug "zsh-users/zsh-history-substring-search", defer:3

zplug "bil-elmoussaoui/flatpak-zsh-completion", defer:3

# theme
#zplug "bhilburn/powerlevel9k", use:powerlevel9k.zsh-theme
zplug "themes/refined", from:oh-my-zsh, as:theme
#zplug "denysdovhan/spaceship-prompt", use:spaceship.zsh, from:github, as:theme

## install plugins if there are plugins that have not been installed
#if ! zplug check --verbose; then
#    printf "Install? [y/N]: "
#    if read -q; then
#        echo; zplug install
#    fi
#fi

# then, source plugins and add commands to $PATH
zplug load
### <ZPLUG/>

### CUSTOM
source ~/.config/aliases
#source /etc/profile.d/vte.sh
source ~/.local/share/nvim/plugged/gruvbox/gruvbox_256palette.sh


# work around Android emulator path problems
function emulator { ( cd "$(dirname "$(whence -p emulator)")" && ./emulator "$@"; ) }

# load pyenv
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
# and the virtualenv plugin
eval "$(pyenv virtualenv-init -)"
